import * as actionTypes from './actionTypes'
import { db } from './../firebase/firebase'

const setUser = (user) => {
    return {
        type: actionTypes.SET_USER,
        currentUser: user
    }
}

const addToCart = (product) => {
    return {
        type: actionTypes.ADD_TO_CART,
        product: product
    }
}

const removeProduct = (product) => {
    return {
        type: actionTypes.REMOVE_PRODUCT,
        product: product
    }
} 

const fetchProductsRequest = () => {
    return {
        type: actionTypes.FETCH_PRODUCTS_REQUEST,
    }
}

const fetchProductsSuccess = (products) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_SUCCESS,
        payload: products
    }
}

const fetchProductsFailure = (error) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_FAILURE,
        error: error,
    }
}

const fetchProducts = () => {
    return (dispatch) => {
        console.log("Product request")
        dispatch(fetchProductsRequest())
        db.collection('products').get().then(response => {
            let products = []
            response.docs.forEach(doc => {
                products.push(doc.data())
            })
            dispatch(fetchProductsSuccess(products))
        }).catch(error => {
            dispatch(fetchProductsFailure(error))
        })
    }
}

export { fetchProducts, fetchProductsRequest, fetchProductsSuccess, fetchProductsFailure, addToCart, removeProduct, setUser }