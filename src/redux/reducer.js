import * as actionTypes from './actionTypes'

const initialState = {
    currentUser: null,
    products: [],
    cart: [],
    loading: false,
    error: '',
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_PRODUCTS_REQUEST:
            return {
                ...state,
                loading: true 
            }

        case actionTypes.FETCH_PRODUCTS_SUCCESS:
            return {
                ...state,
                products: action.payload,
            }

        case actionTypes.FETCH_PRODUCTS_FAILURE:
            return {
                ...state,
                error: action.error
            }

        case actionTypes.ADD_TO_CART:
            return {
                cart: state.cart.push(action.product),
                ...state
            }

        case actionTypes.REMOVE_PRODUCT:
            return {
                ...state,
                cart: state.cart.filter(product => product !== action.product)
            }

        case actionTypes.SET_USER:
            return {
                ...state,
                currentUser: action.currentUser,
            }
    
        default: return state
    }
}

export default reducer