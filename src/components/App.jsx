import React from 'react';
import  { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { auth, db } from '../firebase/firebase'
import { connect } from 'react-redux'
import { setUser } from './../redux/actionCreators'
import './../sass/styles.scss'

import MainPage from './pages/MainPage';
import Cart from './pages/Cart'
import Register from './pages/Register'
import Login from './pages/Login'
import NotFound from './pages/NotFound'


class App extends React.Component {

  componentDidMount(){
    auth.onAuthStateChanged(user => {
      if(user){
        db.collection('users').where('email', '==', `${user.email}`).get()
          .then(response => {
            this.props.setUser(response.docs[0].data())
          })
      }
      else {
        this.props.setUser(user)
      }
    })
  }

  componentDidUpdate() {
    auth.onAuthStateChanged(user => {
      if (user) {
        db.collection('users').where('email', '===', `${user.email}`).get()
          .then(response => {
            setUser(response.docs[0].data())
          })
      }
      else {
        this.props.setUser(user)
      }
    })
  }

  render(){
      return (
        <Router basename={process.env.PUBLIC_URL}>
          <Switch>
            <Route exact path="/">
              <MainPage />
            </Route> 
            <Route path="/cart">
              <Cart />
            </Route>
            <Route path="/register">
              <Register />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <Route component={() => {return <NotFound />}}></Route>
          </Switch>
        </Router>
      );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: (user) => { dispatch(setUser(user)) }
  }
}

export default connect(null, mapDispatchToProps)(App);
