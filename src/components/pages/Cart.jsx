import React from 'react';
import Header from '../molecules/Header'
import Footer from '../molecules/Footer'
import CartList from '../molecules/CartList'

const Cart = () => {
    return (  
        <>
            <Header />
            <CartList />
            <Footer />
        </>
    );
}
 
export default Cart;