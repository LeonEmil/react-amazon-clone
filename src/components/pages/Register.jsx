import React from 'react';
import FormLoginHeader from '../molecules/FormLoginHeader'
import FormRegister from '../molecules/FormRegister'
import FormLoginFooter from '../molecules/FormLoginFooter'

const Register = () => {
    return (  
        <div className="login-page">
            <FormLoginHeader/>
            <FormRegister/>
            <FormLoginFooter/>
        </div>
    );
}
 
export default Register;