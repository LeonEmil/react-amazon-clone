import React from 'react';
import FormLoginHeader from '../molecules/FormLoginHeader'
import FormLogin from '../molecules/FormLogin'
import FormLoginFooter from '../molecules/FormLoginFooter'

const Login = () => {
    return (
        <div className="login-page">
            <FormLoginHeader />
            <FormLogin />
            <FormLoginFooter />
        </div>
    );
}

export default Login;