import React from 'react'

import Header from '../molecules/Header'
import ProductList from '../molecules/ProductList'
import Aside from '../molecules/Aside'
import Footer from '../molecules/Footer'

const MainPage = () => {
    return ( 
        <div className="main-page">
            <Header />
            <ProductList />
            <Aside />
            <Footer />
        </div>
     );
}
 
export default MainPage;