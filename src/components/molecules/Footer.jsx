import React from 'react'
import { footerList } from '../../js/settings/variables'

const Footer = () => {
    return (
        <>
            <footer className="footer">
                <button className="footer__back-to-top-button">Back to top</button>
                <div className="footer__amazon-logo"></div>
                <div className="footer__grid">
                    {
                        footerList.map((element, key)=> {
                            return (
                                <div className="footer__element" key={key}>
                                    <a href={element.link} className="footer__link">
                                        <span className="footer__text-large">{element.title}</span>
                                        <span className="footer__text-small">{element.subtitle}</span>
                                    </a>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="footer__foot">
                    <a href="#home" className="footer__foot-link">Conditions of Use</a>
                    <a href="#home" className="footer__foot-link">Privacy Notice</a>
                    <a href="#home" className="footer__foot-link">Interest-based Ads</a>
                </div>
                <span className="footer__foot-text">© 1996-2020, Amazon.com, Inc. or its affiliates</span>
            </footer>
        </>
    );
}
 
export default Footer;