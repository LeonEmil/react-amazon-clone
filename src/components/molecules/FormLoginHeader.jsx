import React from 'react';
import { Link } from 'react-router-dom'

const FormLoginHeader = () => {
    return (  
        <header className="header-login">
            <Link to="/">
                <div className="header-login__logo">
                    <span className="sr-only">Amazon</span>
                </div>
            </Link>
        </header>
    );
}
 
export default FormLoginHeader;