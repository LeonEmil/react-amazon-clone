import React  from 'react';
import { connect } from "react-redux"
import { removeProduct } from "./../../redux/actionCreators"
import CartSubtotal from "./CartSubtotal"

const CartList = ({cart, removeProduct}) => {
    return cart.length > 0 ? ( 
        <>
        <h1 className="cart__title">Shopping cart</h1>
        <CartSubtotal />
        <main className="cart__list">  
            <div className="cart__price-title">
                <span>Price</span>
            </div>
            {
                cart.map((product, key) => {
                    return (
                        <div className="cart__item" key={key}>
                            <div className="cart__image-container">
                                <img src={product["src-small"]} className="cart__image" alt={product.alt} />
                            </div>
                            <div className="cart__info">
                                <a className="cart__name" href="http://#">{product.name}</a>
                                <span className="cart__publisher">by {product.publisher}</span>
                                <button className="cart__button" onClick={() => { removeProduct(product) }}>Remove from the cart</button>
                            </div>
                            <span className="cart__price">${product.price}</span>
                        </div>
                    )
                })
            }
            
        </main>
        </>
    ) : (
        <div className="cart-empty">
            <h2 className="cart-empty__title">Your cart is empty</h2>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        cart: state.cart,
        counter: state.cart.length
    }
}

const mapDispatchToProps = dispatch => {
    return {
        removeProduct: product => { dispatch(removeProduct(product)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(CartList);