import React, {useState} from 'react';
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { auth, db } from '../../firebase/firebase'
import { useFormik } from 'formik'
import * as Yup from 'yup'


const FormRegister = () => {
    const [redirect, setRedirect] = useState(false)

    
    const {handleSubmit, handleChange, handleBlur,values, touched, errors} = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            confirmPassword: ''
        },
        validationSchema: Yup.object({
            name: Yup.string().max(10, 'Login must be shorter than 10 characters').required('Enter your name'),
            email: Yup.string().required().email().required('Enter your email'),
            password: Yup.string().min(6, 'Password should be longer than 6 characters').required('Enter your password'),
            confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match').required('Type your password again')
        }),
        onSubmit: ({name, email, password}) => {
            auth.createUserWithEmailAndPassword(email, password)
                .then(() => {
                    db.collection('users').add({
                        name: `${name}`,
                        email: `${email}`,
                    }).then(() => {
                        setRedirect(true)
                    })
                })
                .catch(error => {
                    alert(error)
                })
        }
    })

    return redirect ? (<Redirect to="/" />) : (  
        <form className="form-register" onSubmit={handleSubmit}>
            <h1 className="form-register__title">Create Account</h1>

            
            <label htmlFor="register-name" className="form-register__label">Your name</label>
            <input 
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                className="form-register__input" 
                id="register-name"
                name="name"
                type="text" 
                required
            />
            { 
                touched.name && errors.name ? 
                <span className="register-alert">{errors.name}</span> 
                : null 
            }

            
            <label htmlFor="register-email" className="form-register__label">Email</label>
            <input 
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                className="form-register__input" 
                id="register-email"
                name="email"
                type="email"
                required 
            />
            { 
                touched.email && errors.email ? 
                <span className="register-alert">{errors.email}</span> 
                : null
            }

            <label htmlFor="register-password" className="form-register__label">Password</label>
            <input 
                onChange={handleChange}
                onBlur={handleBlur}
                className="form-register__input" 
                value={values.password}
                id="register-password"
                name="password"
                type="password" 
                required
            />
            { 
                touched.password && errors.password ? 
                <span className="register-alert">{errors.password}</span> 
                : null
            }

            <span className="register-password-alert" id="register-password-alert"></span>
            <label htmlFor="register-confirm-password" className="form-register__label">Re-enter password</label>
            <input 
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.confirmPassword}            
                className="form-register__input" 
                id="register-confirm-password" 
                name="confirmPassword"
                type="password"
                required
            />
            { 
                touched.confirmPassword && errors.confirmPassword ? 
                <span className="register-alert">{errors.confirmPassword}</span> 
                : null
            }

            <input type="submit" value="Create your amazon account" className="form-login__button"/>
        </form>
    )
}
 
export default connect(null, null)(FormRegister);