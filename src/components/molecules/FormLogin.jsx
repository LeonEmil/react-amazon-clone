import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom'
import { auth } from '../../firebase/firebase'
import { useFormik } from 'formik'
import * as Yup from 'yup'

const FormLogin = () => {

    const [redirect, setRedirect] = useState(false)

    const {handleSubmit, handleChange, handleBlur, values, errors, touched} = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string().email().required('Please enter you email'),
            password: Yup.string().required('Please enter your password')
        }),
        onSubmit: ({email, password}) => {
            auth.signInWithEmailAndPassword(email, password)
            .then(() => {
                setRedirect(true)
            })
            .catch(error => {
                alert(error)
            })
        }
    })

    return redirect ? (<Redirect to="/"/>) : (  
        <>
        <form className="form-login" onSubmit={handleSubmit}>
            <h1 className="form-signin__title">Sign in</h1>

            <label htmlFor="email" className="form-login__label">Email</label>
            <input 
                onChange={handleChange}
                onBlur={handleBlur}
                type="email" 
                className="form-login__input" 
                id="login-email"
                name="email"
                value={values.email}
                required
            />
            {
                errors.email && touched.email ? 
                <span className="login-alert">{errors.email}</span>
                : null
            }

            <label htmlFor="password" className="form-login__label">Password</label>
            <input 
                onChange={handleChange}
                onBlur={handleBlur}
                type="password" 
                className="form-login__input" 
                id="login-password"
                name="password"
                value={values.password}
                required
            />
            {
                errors.password && touched.password ? 
                <span className="login-alert">{errors.password}</span>
                : null
            }
            
            <input 
                type="submit" 
                value="Continue" 
                className="form-login__button"
            />
        </form>
        <div className="login__new-container">
            <h2 className="login__new">New in Amazon?</h2>
            <Link to="/register">
                <button className="login__new-button">Create an amazon account</button>
            </Link>
        </div>
        </>
    );
}

 
export default FormLogin;