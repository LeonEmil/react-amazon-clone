import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { addToCart, fetchProducts } from '../../redux/actionCreators'
import ClipLoader from 'react-spinners/ClipLoader'
//import Product from './Product';

const ProductList = ({ fetchProducts, products, addToCart }) => {
    
    useEffect(() => {
        fetchProducts()
        
    }, [fetchProducts])
    
    return products.length > 0 ? (
        <main className="product__grid"> 
            {
                products.map((product, key) => {
                    return(
                        <div className="product__item" key={key}>
                            <div className="product__image-container">
                                <img src={product["src-small"]} className="product__image" alt={product.alt} />
                            </div>
                            <a className="product__name" href="http://#">{product.name}</a>
                            <span className="product__publisher">by {product.publisher}</span>
                            <span className="product__price">${product.price}</span>
                            <button className="product__button" onClick={() => { addToCart(product) }}>Add to cart</button>
                        </div>
                    )
                })
            }
        </main>
    ) : (<div className="loader"><ClipLoader /></div>) 
   
}

const mapStateToProps = state => {
    return {
        products: state.products,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchProducts: () => { dispatch(fetchProducts()) },
        addToCart: (product) => { dispatch(addToCart(product)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(ProductList);