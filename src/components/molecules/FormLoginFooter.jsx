import React from 'react';

const FormLoginFooter = () => {
    const year = new Date().toDateString().slice(11)

    return (  
        <footer className="footer-login">
            <ul className="footer-login__list">
                <li className="footer-login__list-item">Contitions of use</li>
                <li className="footer-login__list-item">Privacity notice</li>
                <li className="footer-login__list-item">Help</li>
            </ul>
            <span className="footer-login__copyright">© 1996-{year}, Amazon.com, Inc. or its affiliates</span>
        </footer>
    );
}
 
export default FormLoginFooter;