import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
//import { setUser } from '../../redux/actionCreators'
import { auth } from '../../firebase/firebase'

const Header = ({currentUser, counter}) => {

    const signOut = () => {
        auth.signOut()
            .then(() => {
                window.location = "/"
            })
    }

    return ( 
        <header className="header">
                <nav className="header__nav">
                    <div className="header__toggle-menu-icon"></div>
                    <Link to="/"><div className="header__amazon-logo"></div></Link>
                    <form action="" className="header__search-form">
                        <label htmlFor="search" className="sr-only">Search</label>
                        <input className="header__nav-search" type="search" name="search" id=""/>
                    </form>
                    {
                        currentUser ? 
                            <>
                                <div className="header__account-button" onClick={ signOut }>
                                    <span className="header__account-button-small-text">Hello, {currentUser.name}</span>
                                    <span className="header__account-button-large-text" >Logout</span>
                                </div> 
                            </>
                            :
                            <>
                                <Link to="/login" className="header__account-button">
                                    <span className="header__account-button-small-text">Hello, sign in</span>
                                    <span className="header__account-button-large-text">Account & List</span>
                                </Link> 
                            </>
                    }
                    
                        <Link to="/cart" className="header__cart-button">
                            <div className="header__cart-button-icon">
                                <span className="header__cart-button-counter">{counter}</span>
                            </div>
                            <span className="header__cart-button-text">Cart</span>
                        </Link>
                </nav>
            <div className="header__image"></div>
        </header>
    );
}

const mapStateToProps = state => {
                                
    return {
        currentUser: state.currentUser,
        counter: state.cart.length
    }
}
 
export default connect(mapStateToProps, null)(Header);