import React from 'react';
import { connect } from 'react-redux'

const CartSubtotal = ({cart}) => {
    
    const price = cart.reduce((total, product) => { 
        return total + product.price
    }, 0)

    return (
        <div className="cart__subtotal">
            <h2 className="cart__subtotal-title">Subtotal ({cart.length} item): ${price}</h2>
            <button className="cart__subtotal__button">Proceed to checkout</button>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        cart: state.cart
    }
}
 
export default connect(mapStateToProps, null)(CartSubtotal);