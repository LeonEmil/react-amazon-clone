import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyCsLSwFLQGlJ5-ekzPsbM3bBp2n7QL2_L8",
    authDomain: "clone-5f939.firebaseapp.com",
    databaseURL: "https://clone-5f939.firebaseio.com",
    projectId: "clone-5f939",
    storageBucket: "clone-5f939.appspot.com",
    messagingSenderId: "660491760511",
    appId: "1:660491760511:web:5e8e9585a8c0f0bccb604a"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth()
export const storage = firebase.storage()
export const db = firebase.firestore()

export default firebase