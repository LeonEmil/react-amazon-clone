const footerList = [
    {
        title: "Amazon music",
        subtitle: "Stream millons of songs",
        link: "https://music.amazon.com/home?ref=dm_aff_amz_com"
    },
    {
        title: "Amazon Advertising",
        subtitle: "Find, attract and engage costumers",
        link: "https://advertising.amazon.com/?ref=footer_advtsing_amzn_com"
    },
    {
        title: "Amazon drive",
        subtitle: "Cloud storage from Amazon",
        link: "https://www.amazon.com/STRING-subnav_primephotos_amazondrive/b?ie=UTF8&node=15547130011&ref_=us_footer_drive"
    },
    {
        title: "6pm",
        subtitle: "Score deals and fashion brands",
        link: "https://www.6pm.com/"
    },
    {
        title: "AbeBooks",
        subtitle: "Books, arts and collectibles",
        link: "https://www.abebooks.com/"
    },
    {
        title: "ACX",
        subtitle: "Audio Books publishing make easy",
        link: "https://www.acx.com/"
    },
    {
        title: "Alexa",
        subtitle: "Axionable Analytics for the web",
        link: "https://www.alexa.com/"
    },
    {
        title: "Sell on Amazon",
        subtitle: "Start a seller account",
        link: "https://sell.amazon.com/?ld=AZUSSOA-footer-aff&ref_=footer_sell"
    },
    {
        title: "Amazon Business",
        subtitle: "Everything for your business",
        link: "https://business.amazon.com/en/home?_encoding=UTF8&ref_=footer_retail_b2b"
    },
    {
        title: "Amazon Global",
        subtitle: "Ship orders internationally",
        link: "https://www.amazon.com/International-Shipping-Direct/b?ie=UTF8&node=230659011&ref_=footer_amazonglobal"
    },
    {
        title: "Home services",
        subtitle: "Experiences pros happiness guarantee",
        link: "https://www.amazon.com/gp/browse.html/ref=vas_sf_load_?node=8098158011"
    },
    {
        title: "Amazon ignite",
        subtitle: "Sell your original digital educational resources",
        link: "https://ignite.amazon.com/?ref=amazon_footer_ignite"
    },
    {
        title: "Amazon rapids",
        subtitle: "Fun stories for kids on the go",
        link: "https://rapids.amazon.com/?ref=rapids_acq_gatewayfooter"
    },
    {
        title: "Amazon web services",
        subtitle: "Scalable cloud computer services",
        link: "https://aws.amazon.com/es/what-is-cloud-computing/?sc_channel=EL&sc_campaign=amazonfooter"
    },
    {
        title: "Audible",
        subtitle: "Listen to books & original audio performance",
        link: "https://www.audible.com/"
    },
    {
        title: "Book depository",
        subtitle: "Books with free delivery wordwide",
        link: "https://www.bookdepository.com"
    },
    {
        title: "Box office mojo",
        subtitle: "Find movie box office data",
        link: "https://www.boxofficemojo.com/?ref_=amzn_nav_ftr"
    },
    {
        title: "ComiXology",
        subtitle: "Thousands of digital comics",
        link: "https://www.comixology.com/"
    },
    {
        title: "CreateSpace",
        subtitle: "Indie print publishing made easy",
        link: "https://www.createspace.com/"
    },
    {
        title: "DPRview",
        subtitle: "Digital photography",
        link: "https://www.dpreview.com/"
    },
    {
        title: "East dane",
        subtitle: "Designer men's fashion",
        link: "https://www.eastdane.com/welcome"
    },
    {
        title: "Fabric",
        subtitle: "Sewing, quilting & knitting",
        link: "https://www.fabric.com/"
    },
    {
        title: "Goodreads",
        subtitle: "Book reviews & recomendations",
        link: "https://www.goodreads.com/"
    },
    {
        title: "IMDb",
        subtitle: "Movies, TV & celebrities",
        link: "https://www.imdb.com/"
    },
    {
        title: "IMDbPro",
        subtitle: "Get info entertainment professional needs",
        link: "https://pro.imdb.com/?ref_=amzn_nav_ftr"
    },
    {
        title: "Kindle direct publishing",
        subtitle: "Indie digital publishing made easy",
        link: "https://kdp.amazon.com/"
    },
    {
        title: "Prime video direct",
        subtitle: "Video distribution made easy",
        link: "https://videodirect.amazon.com/home/landing"
    },
    {
        title: "Shopbop",
        subtitle: "Designer fashion brands",
        link: "https://www.shopbop.com/welcome"
    },
    {
        title: "Woot!",
        subtitle: "Deals and shenanigans",
        link: "https://www.woot.com/"
    },
    {
        title: "Zappos",
        subtitle: "Shoes and clothing",
        link: "https://www.zappos.com/"
    },
    {
        title: "Ring",
        subtitle: "Smart home segurity systems",
        link: "https://ring.com/"
    },
    {
        title: "eero WIFI",
        subtitle: "Stream 4K video in every room",
        link: "https://eero.com/"
    },
    {
        title: "Neighbors app",
        subtitle: "Real-time crime & safety alerts",
        link: "https://shop.ring.com/pages/neighbors-app"
    },
    {
        title: "Amazon subscription boxes",
        subtitle: "Top subscription boxes - right to your door",
        link: "https://www.amazon.com/b?ie=UTF8&node=14498690011&ref_=amzn_nav_ftr_swa"
    },
    {
        title: "PillPack",
        subtitle: "Pharmacy simplificied",
        link: "https://www.pillpack.com/"
    },
    {
        title: "Amazon second chance",
        subtitle: "Pass it on, trade it in, give it a a second life",
        link: "https://www.amazon.com/amazonsecondchance?_encoding=UTF8&ref_=footer_asc"
    },
]

export { footerList }